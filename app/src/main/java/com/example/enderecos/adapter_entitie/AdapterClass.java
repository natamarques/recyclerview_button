package com.example.enderecos.adapter_entitie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.enderecos.R;

public class AdapterClass extends RecyclerView.Adapter<AdapterClass.MyViewHolder> {


    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public AdapterClass.MyViewHolder onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull AdapterClass.MyViewHolder holder, int position) {
        holder.endereco.setText("Av Pedro Álvares Cabral");
    }

    @Override
    public int getItemCount() {
        return 20;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView endereco;

        public MyViewHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);
            endereco = itemView.findViewById(R.id.address_info);
        }
    }
}
